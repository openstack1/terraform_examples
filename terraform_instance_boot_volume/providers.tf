provider "openstack" {
  # Ваша учетная запись.
  user_name = var.user_name
  password = file(var.credentials)
  tenant_id = var.tenant_id
  
  # Индикатор расположения пользователей.
  user_domain_id = "users"
  # API endpoint Через данный адрес terraform будет обращаться в api mcs.
  auth_url = "https://example.ru/api/v1/"
}
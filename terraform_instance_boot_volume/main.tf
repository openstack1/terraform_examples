# creates an openstack volume

resource "openstack_blockstorage_volume_v2" "volume" {
  name        = "${var.project_name}-vol"
  size        = var.hdd_size
#  image_id    = "206ea6ae-fbec-4b80-8951-93af1b44dbac"
  image_id  = var.image_id
  volume_type = "${var.datacenter}-ssd01"
}

resource "openstack_compute_instance_v2" "instance_1" {
  name            = "${var.project_name}-${var.instance_name}"
  #flavor_id         = "13df6078-8733-4f03-9682-824884c69b76"
  flavor_name       = var.flavor_name
  key_pair          = var.key_pair # optional
  availability_zone = "${var.datacenter}1" 
  security_groups   = var.security_groups

  block_device {
    uuid                  = openstack_blockstorage_volume_v2.volume.id
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    name = var.network_name
#    fixed_ip_v4 = var.fixed_ip_v4 #(optional)
  }
}

output "example_instance_ip" {
  value = openstack_compute_instance_v2.instance_1.access_ip_v4
}
#connect to api endpoint openstack
credentials        = "../creds/serviceaccount"
user_name          = "andrey.talabirchuk"
tenant_id          = "64acce8bdee64ee9b32c8d314e136699"

#environment for VM
project_name       = "t-tlbr"
hdd_size           = 15
image_id           = "206ea6ae-fbec-4b80-8951-93af1b44dbac"
image_name         = "x5-Centos_7.8" # x5-Centos_7.8, x5-Centos_8.1, x5-Fedora-Atomic-28-v1.16.4-mcs.1, x5-Ubuntu_18.04
datacenter         = "dpro" # dpro-ssd01, dtln-ssd01
instance_name      = "tlb-bo2" # max 8 symbols
flavor_name        = "Basic-1-2-20" #Basic-1-1-10, Standard-4-16-50, Standard-2-4-40
key_pair           = "talabirchuk"
security_groups    = [
    "6b1032a6-7f41-4c51-89dd-3a7ba2f90e29", 
    "a88c6074-cf44-4b34-a71b-9ec926f3be16"
    ]
network_name       = "10-DEV" # 30-PREPROD, 20-TEST, 40-PROD
fixed_ip_v4        = "192.168.84.19" # development net = 192.168.84.0/23 test net = 192.168.86.0/23 pre-prod-net=192.168.88.0/23 
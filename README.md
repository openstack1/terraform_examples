# terraform
## Примеры использования terraform в нашем приватном облаке X5
- [примеры с сайта terraform](https://www.terraform.io/docs/providers/openstack/r/compute_instance_v2.html#example-usage)
- обязательный фаил с паролем creds/serviceaccount 
- main.tf - фаил где прописывается сценарий создания инфраструктуры
- variables.auto.tfvars - фаил где прописываются переменные для удобства использования
- variables.tf - фаил определения переменных и какие у них параметры
- providers.tf - фаил где хранятся все ключи доступа к окружению и какого провайдера использовать

## terraform-single-instance
- создание обычной Виртуальной машины
## terraform_instance_second_volume
- создание обычной Виртуальной машины и подключение второго volume /dev/vdb1
## terraform_instance_boot_volume
- создание обычной Виртуальной машины, создание отдельного диска и подключение его как загрузочного

пример использования:
1. cd terraform-single-instance
2. terraform init - инициализация файла providers.tf загрузка необходимыых модулей (обязательное подключение к интернету)
3. terrform plan - план конфигурации, можно посмотреть, что применится
4. terraform apply - применения плана конфигурации и деплой конфигурации в облако
5. terraform destroy - удаление конфигурации из облака по созданному плану
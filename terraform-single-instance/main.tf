# creates an instance based on CentOS 7.7 image in private network
resource "openstack_compute_instance_v2" "single-instance" {
  name            = "${var.project_name}-${var.instance_name}"
  image_id        = var.image_id
  flavor_name     = var.flavor_name
  key_pair        = var.key_pair # optional

  security_groups = var.security_groups

  network {
    name = var.network_name
#    fixed_ip_v4 = var.fixed_ip_v4 #(optional)
  }
}

output "example_instance_ip" {
  value = openstack_compute_instance_v2.single-instance.access_ip_v4
}
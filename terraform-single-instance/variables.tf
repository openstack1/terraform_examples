variable "credentials" {
  type        = string
  description = "Location of the credentials keyfile, in your file the password"
}

variable "user_name" {
  type        = string
  description = "You LDAP user name. example: andrey.talabirchuk"
}

variable "tenant_id" {
  type        = string
  description = "You project tenant_id look file env OS_PROJECT_ID. example OS_PROJECT_ID=64acce8bdee64ee9b32c8d314e136699 "
}

variable "project_name" {
  type        = string
  description = "project name on "
}

variable "hdd_size" {
  type        = number
  description = "HDD SIZE. Must be >=10 and."
}

variable "image_id" {
  type        = string
  description = "image id: openstack image list "
}

variable "image_name" {
  type        = string
  description = "image_name: openstack image list "
}

variable "datacenter" {
  type        = string
  description = "datacenter, dpro || dtln"
}

variable "instance_name" {
  type        = string
  description = "instance_name, max 8 symbols"
}

variable "flavor_name" {
  type        = string
  description = "flavor_name, get flavor name: openstack flavor list"
}

variable "key_pair" {
  type        = string
  description = "key_pair, get key_pair name(optional): openstack keypair list"
}

variable "security_groups" {
  type        = list(string)
  description = "The list of security group. How to get security group: openstack security group list"
}

variable "network_name" {
  type        = string
  description = "network_name, get network_name name: openstack network list"
}

variable "fixed_ip_v4" {
  type        = string
  description = "network_name, get network_name name: openstack network list"
}

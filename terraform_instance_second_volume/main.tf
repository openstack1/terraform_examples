# creates an openstack volume
resource "openstack_blockstorage_volume_v2" "volume" {
  name        = "${var.project_name}-vol"
  size        = var.hdd_size
  volume_type = "${var.datacenter}-ssd01"
}

resource "openstack_compute_instance_v2" "instance_2" {
  name              = "${var.project_name}-${var.instance_name}"
  image_id          = var.image_id
  flavor_name       = var.flavor_name
  #flavor_id         = "13df6078-8733-4f03-9682-824884c69b76"  
  key_pair          = var.key_pair # optional
  availability_zone = "${var.datacenter}1" 
  security_groups   = var.security_groups

  network {
    name = var.network_name
  }
}

resource "openstack_compute_volume_attach_v2" "attached" {
  instance_id = "${openstack_compute_instance_v2.instance_2.id}"
  volume_id   = "${openstack_blockstorage_volume_v2.volume.id}"
}

output "example_instance_ip" {
  value = openstack_compute_instance_v2.instance_2.access_ip_v4
}
